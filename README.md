# tastyspleen-player-stats-scraper

This service scrapes the tastyspleen.net website for Quake2 game statistics for a given user.

## Installing

1. Clone this repository 
   - `$ git clone https://gitlab.com/aldgagnon/tastyspleen-player-stats-scraper.git`
   
2. Start the Docker container
   - `$ docker-compose up --build -d`

## Usage

The service can be queried to obtain a player's statistics using the following curl format.

```
curl localhost:5000\<username>
```

An example curl command.

```
curl localhost:5000\jerko
```

