const express = require('express');
const tsScraper = require('tastyspleen-stats-scraper');

const router = express.Router();

const logger = require('./winston');

// Define GET endpoint
router.get('/:playerName', (req, res) => {
  tsScraper(req.params.playerName).then((results) => {
    logger.debug(`Results found for player ${req.params.playerName}`);
    res.send(results);
  }).catch((error) => {
    logger.error(`Error scraping tastyspleen - ${error}`);
    res.status(500).end();
  });
});

module.exports = router;
