const { createLogger, format, transports } = require('winston');

const logger = createLogger({
  level: 'debug',
  format: format.combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    format.colorize(),
    format.simple(),
    format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`),
  ),
  defaultMeta: { service: 'tastyspleen-player-stats-scraper' },
  transports: [
    new transports.Console(),
  ],
});

module.exports = logger;
